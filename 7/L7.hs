
import Text.Read

a1 :: [Int]
a1 = return 1

a2 = [1,2] >>= (\a -> [a,a+1])

data Log a = Log [String] a
             deriving Show

withLog :: a -> String -> Log a
withLog = undefined

twoLogs :: a -> (a -> Log b) -> (b -> Log c) -> Log c
twoLogs = undefined

{-
> twoLogs 1
          (\a -> (a+1) `withLog` "add 1")
          (\b -> (b*2) `withLog` "mult 2")
Log ["add 1","mult 2"] 4
-}

instance Monad Log where
  return a = Log [] a
  (Log l1 a) >>= f = Log (l1 ++ l2) b
    where Log l2 b = f a

instance Functor Log where
  fmap f (Log l a) = Log l $ f a

instance Applicative Log where
  pure a = Log [] a
  (Log l1 f) <*> (Log l2 a) = Log (l1 ++ l2) $ f a

logged1 = return 1 >>=
          \a -> (a+1) `withLog` "add 1" >>=
                \b -> (b*2) `withLog` "mult 2"

logged2 = do
  a <- return 1
  b <- (a+1) `withLog` "add 1"
  (b*2) `withLog` "mult 2"

data Optional a = NoParam
                | Param a
                deriving Show

instance Functor Optional where

instance Applicative Optional where

instance Monad Optional where

testOptional :: String -> Optional Int
testOptional input = do
  s <- if input == "" then NoParam else Param input
  x <- maybe NoParam Param $ readMaybe s
  return $ x*2

